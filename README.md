## leftpad

Like the [pad module](https://github.com/wdavidw/node-pad), except I'll remember
the argument order.

```js
var leftpad = require('leftpad');

leftpad(5, 10);
'0000000005'
```
